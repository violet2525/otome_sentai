【ティラノスタジオ用デバッグ支援プラグイン】

■使い方
first.ksとかに以下のように記述してください。
記述した時点からデバッグツールがゲーム画面上に表示されます。

[plugin name=tsex]
指定可能属性
key：ホットキーを変更できます。指定の仕方はkeyConfig.jsに書いてあるURL見てください


■できること
・ホットキーによるゲーム画面リロード
Shift+Alt+Rキー同時押しによりゲーム画面をリロードできます。

・シナリオジャンプ
プロジェクト内の任意のシナリオにジャンプできます。
ジャンプ時に画面初期化も行います。

・ここまでスキップ
ジャンプしたシナリオ内の任意の行までスキップします。

・ひとつ戻る
ひとつ前のメッセージまで戻ります。

・ミュート
ゲーム画面自体をミュートします。
（BGM音量等は変化しません）

・オート（おまけ）
チェックを入れるとオートモードがスタートします。

・スキップ（おまけ）
チェックを入れるとスキップモードがスタートします。


あとは自分で使ってみて理解してください

