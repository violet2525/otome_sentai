//
// ティラノスクリプトのタグ拡張は原則ここに記述
//


$.getNowDate = function(){
    var nowdate = new Date();
    // 年
    var year = nowdate.getFullYear();
    // 月
    var mon = nowdate.getMonth() + 1;
    mon = ("000" + mon).slice(-2)
    // 日
    var date = nowdate.getDate();
    date = ("000" + date).slice(-2)

    return year + "/" + mon + "/" + date
}
//現在の時刻
$.getNowTime = function() {

    var nowdate = new Date();

    var h = nowdate.getHours();
    h = ("000" + h).slice(-2)
    var m = nowdate.getMinutes();
    m = ("000" + m).slice(-2)
    var s = nowdate.getSeconds();
    s = ("000" + s).slice(-2)

    return h + ":" + m + ":" + s;

};

tyrano.plugin.kag.tmp.map_is_audio_stopping = {}
tyrano.plugin.kag.tmp.map_is_audio_wait = {}

//クリック待ち記号
tyrano.plugin.kag.ftag.showNextImg = function () {
    if (this.kag.stat.flag_glyph == "false") {
        $(".img_next").remove();
        var jtext = this.kag.getMessageInnerLayer();
        jtext.find("p").append("<img class='img_next' src='./data/image/nextpage.png' />")
    } else $(".glyph_image").show()
}

//showMessage拡張
/*
var _text = tyrano.plugin.kag.tag.text
var _kag = tyrano.plugin.kag.ftag.master_tag.text.kag
tyrano.plugin.kag.tag.text = $.extend(true, {} , _text, {
    showMessage: function (message_str, pm, isVertical) {
        var that = this;
        if (that.kag.stat.log_join == "true") pm.backlog = "join";
        var chara_name = $.isNull($(".chara_name_area").html());
        if (chara_name != "" && pm.backlog != "join" || chara_name != "" && this.kag.stat.f_chara_ptext == "true") {
          this.kag.pushBackLog("<p class='backlog_chara_name " + chara_name + "'>" + chara_name + "</p><p class='backlog_text " + chara_name + "'>" + message_str + "</p>", "add");
          if (this.kag.stat.f_chara_ptext == "true") {
            this.kag.stat.f_chara_ptext = "false";
            this.kag.stat.log_join = "true"
          }
        } else {
          var log_str = "<p class='backlog_chara_name'></p><p class='backlog_text " + chara_name + "'>" + message_str + "</p>";
          if (pm.backlog == "join") this.kag.pushBackLog(log_str, "join");
          else this.kag.pushBackLog(log_str, "add")
        }
        if (that.kag.stat.play_speak == true) speechSynthesis.speak(new SpeechSynthesisUtterance(message_str));
        that.kag.ftag.hideNextImg();
        (function (jtext) {
          if (jtext.html() == "")
            if (isVertical) jtext.append("<p class='vertical_text'></p>");
            else jtext.append("<p class=''></p>");
          var current_str = "";
          if (jtext.find("p").find(".current_span").length != 0) current_str = jtext.find("p").find(".current_span").html();
          that.kag.checkMessage(jtext);
          var j_span = that.kag.getMessageCurrentSpan();
          j_span.css({
            "color": that.kag.stat.font.color,
            "font-weight": that.kag.stat.font.bold,
            "font-size": that.kag.stat.font.size + "px",
            "font-family": that.kag.stat.font.face,
            "font-style": that.kag.stat.font.italic
          });
          if (that.kag.stat.font.edge != "") {
            var edge_color = that.kag.stat.font.edge;
            j_span.css("text-shadow", "1px 1px 0 " + edge_color + ", -1px 1px 0 " + edge_color + ",1px -1px 0 " + edge_color + ",-1px -1px 0 " + edge_color + "")
          } else if (that.kag.stat.font.shadow != "") j_span.css("text-shadow", "2px 2px 2px " + that.kag.stat.font.shadow);
          if (that.kag.config.autoRecordLabel == "true")
            if (that.kag.stat.already_read == true) {
              if (that.kag.config.alreadyReadTextColor != "default") j_span.css("color", $.convertColor(that.kag.config.alreadyReadTextColor))
            } else if (that.kag.config.unReadTextSkip == "false") that.kag.stat.is_skip = false;
          var ch_speed = 30;
          if (that.kag.stat.ch_speed != "") ch_speed = parseInt(that.kag.stat.ch_speed);
          else if (that.kag.config.chSpeed) ch_speed = parseInt(that.kag.config.chSpeed);
          var append_str = "";
          for (var i = 0; i < message_str.length; i++) {
            var c = message_str.charAt(i);
            if (that.kag.stat.ruby_str != "") {
              c = "<ruby><rb>" + c + "</rb><rt>" + that.kag.stat.ruby_str + "</rt></ruby>";
              that.kag.stat.ruby_str = ""
            }
            append_str += "<span style='visibility: hidden'>" + c + "</span>"
          }
          current_str += "<span>" + append_str + "</span>";
          that.kag.appendMessage(jtext, current_str);
          var append_span = j_span.children("span:last-child");
          var makeVisible = function (index) {
            append_span.children("span:eq(" + index + ")").css("visibility", "visible")
          };
          var makeVisibleAll = function () {
            append_span.children("span").css("visibility", "visible")
          };
          var pchar = function (index) {
            var isOneByOne = that.kag.stat.is_skip != true && that.kag.stat.is_nowait != true && ch_speed >= 3;
            if (isOneByOne) makeVisible(index);
            if (index <= message_str.length) {
              that.kag.stat.is_adding_text = true;
              if (that.kag.stat.is_click_text == true || that.kag.stat.is_skip == true || that.kag.stat.is_nowait == true) pchar(++index);
              else setTimeout(function () {
                pchar(++index)
              }, ch_speed)
            } else {
              that.kag.stat.is_adding_text = false;
              that.kag.stat.is_click_text = false;
              if (that.kag.stat.is_stop != "true")
                if (!isOneByOne) {
                  makeVisibleAll();
                  setTimeout(function () {
                    if (!that.kag.stat.is_hide_message) that.kag.ftag.nextOrder()
                  }, parseInt(that.kag.config.skipSpeed))
                } else if (!that.kag.stat.is_hide_message) that.kag.ftag.nextOrder()
            }
          };
          pchar(0)
        })(this.kag.getMessageInnerLayer())
    }.bind(TYRANO)
})
tyrano.plugin.kag.ftag.master_tag.text = tyrano.plugin.kag.tag.text
tyrano.plugin.kag.ftag.master_tag.text.kag = _kag
*/

//[loadcss]変更
/*
var _loadcss = tyrano.plugin.kag.tag.loadcss
var _kag = tyrano.plugin.kag.ftag.master_tag.loadcss.kag
tyrano.plugin.kag.tag.loadcss = $.extend(true, {} , _loadcss, {
    start : function(pm) {
        var file = pm.file;
        //ファイルの読み込み
        var style = '<link rel="stylesheet" href="' + file + "?" + Math.floor(Math.random() * 10000000) + '">';
        $('link:last').after(style);
        this.kag.stat.cssload[file] = true;
        this.kag.ftag.nextOrder();
    }
})
tyrano.plugin.kag.ftag.master_tag.loadcss = tyrano.plugin.kag.tag.loadcss
tyrano.plugin.kag.ftag.master_tag.loadcss.kag = _kag
*/

//[button]タグ拡張
/*
var _button = tyrano.plugin.kag.tag.button
var _kag = tyrano.plugin.kag.ftag.master_tag.button.kag
tyrano.plugin.kag.tag.button = $.extend(true, {} , _button, {
    start: function (pm) {
        var that = this;
        var target_layer = null;
        if (pm.role != "") pm.fix = "true";
        if (pm.fix == "false") {
            target_layer = this.kag.layer.getFreeLayer();
            target_layer.css("z-index", 999999)
        } else target_layer = this.kag.layer.getLayer("fix");
        var storage_url = "";
        if ($.isHTTP(pm.graphic)) storage_url = pm.graphic;
        else storage_url = "./data/" + pm.folder + "/" + pm.graphic;
        var j_button = $("<img />");
        //j_button.attr("src", storage_url);
        j_button.css("position", "absolute");
        j_button.css("cursor", "pointer");
        j_button.css("z-index", 99999999);
        //追加ここから
        var temp_img = $("<img />")
        temp_img.attr("src", storage_url)
        temp_img.on("load", function(){
            j_button.css({
                background: "url(" + storage_url + ")",
                width: temp_img[0].naturalWidth,
                height: temp_img[0].naturalHeight,
                transition: "0.2s"
            })
        })
        j_button.attr("src", "./data/image/blank.png");
        //追加ここまで
        if (pm.visible == "true") j_button.show();
        else j_button.hide();
        if (pm.x == "") j_button.css("left", this.kag.stat.locate.x + "px");
        else j_button.css("left", pm.x + "px");
        if (pm.y == "") j_button.css("top", this.kag.stat.locate.y + "px");
        else j_button.css("top", pm.y + "px");
        if (pm.fix != "false") j_button.addClass("fixlayer");
        if (pm.width != "") j_button.css("width", pm.width + "px");
        if (pm.height != "") j_button.css("height", pm.height + "px");
        if (pm.hint != "") j_button.attr({
            "title": pm.hint,
            "alt": pm.hint
        });
        $.setName(j_button, pm.name);
        that.kag.event.addEventElement({
            "tag": "button",
            "j_target": j_button,
            "pm": pm
        });
        target_layer.append(j_button);
        that.setEvent(j_button, pm);
        if (pm.fix == "false") target_layer.show();
        this.kag.ftag.nextOrder()
    },
    setEvent: function (j_button, pm) {
        var that = TYRANO;
        var _pm = pm
        
        if(j_button.attr("class").indexOf("btn_disable") >= 0){
            return false
        }
        //本来の処理
        _button.setEvent.apply(TYRANO, arguments)
        var evClick = []
        var evOver = []
        var evOut = []
        //現在のイベント取得
        var ev = $._data(j_button.get(0), "events")
        ev.click.forEach(function(k) {
            evClick.push(k.handler);
        });
        ev.mouseover.forEach(function(k) {
            evOver.push(k.handler);
        });
        ev.mouseout.forEach(function(k) {
            evOut.push(k.handler);
        });
        //一旦イベント削除
        j_button.off("click")
        j_button.off("hover")

        //イベント再登録
        j_button.hover(function (event) {
            //ホバー開始時
            //ホバー無効化ここから
            if(that.kag.embScript(_pm.enterdis)){
                return false
            }
            //ホバー無効化ここまで
            //enterimg処理ここから
            if(_pm.over !== undefined){
                j_button.css({
                    background: "url(./data/image/" + _pm.over + ")"
                })    
            }
            //enterimg処理ここまで
            //本来の処理
            evOver.forEach(function(e){
                e.apply(TYRANO, [event])
            })
        }, function (event) {
            //ホバー終了時
            //ホバー無効化ここから
            if(that.kag.embScript(_pm.leavedis)){
                return false
            }
            //ホバー無効化ここまで
            //enterimgここから
            if(_pm.over !== undefined){
                j_button.css({
                    background: "url(./data/image/" + _pm.graphic + ")"
                })
            }
            //enterimgここまで
            //本来の処理
            evOut.forEach(function(e){
                e.apply(TYRANO, [event])
            })
        })
        j_button.click(function (event) {
            //クリック無効化ここから
            if(TYRANO.kag.stat.is_strong_stop == true && j_button.hasClass("button_close")){
                return false
            }
            //クリック無効化ここまで
            //本来の処理
            evClick.forEach(function(e){
                e.apply(TYRANO, [event])
            })
        })
    }
})
tyrano.plugin.kag.ftag.master_tag.button = tyrano.plugin.kag.tag.button
tyrano.plugin.kag.ftag.master_tag.button.kag = _kag
*/

//オーディオ再生拡張
/*
TYRANO.kag.tmp.map_audio_id = {}
var _playbgm = tyrano.plugin.kag.tag.playbgm
var _kag = tyrano.plugin.kag.ftag.master_tag.playbgm.kag
tyrano.plugin.kag.tag.playbgm = $.extend(true, {} , _playbgm, {
    vital : ["storage"],

    pm : {
        loop : "true",
        storage : "",
        fadein : "false",
        time : 2000,
        volume : "",
        buf:"0",
        target : "bgm", //"bgm" or "se"
        sprite_time:"", //200-544 
        click : "false", //音楽再生にクリックが必要か否か
        stop : "false" //trueの場合自動的に次の命令へ移動しない。ロード対策
    },

    start : function(pm) {
        var that = this;
        if (pm.target == "bgm" && that.kag.stat.play_bgm == false) {
            that.kag.ftag.nextOrder();
            return;
        }
        if (pm.target == "se" && that.kag.stat.play_se == false) {
            that.kag.ftag.nextOrder();
            return;
        }
        //スマホアプリの場合
        if ($.userenv() != "pc") {
            this.kag.layer.hideEventLayer();
            if (this.kag.stat.is_skip == true && pm.target == "se") {
                that.kag.layer.showEventLayer();
                that.kag.ftag.nextOrder();
            } else {
                //スマホからのアクセスで ready audio 出来ていない場合は、クリックを挟む
                if(this.kag.tmp.ready_audio == false){
                    $(".tyrano_base").on("click.bgm", function() {
                        that.kag.readyAudio();
                        that.kag.tmp.ready_audio = true;
                        that.play(pm);
                        $(".tyrano_base").off("click.bgm");
                    });
                }else{
                    that.play(pm);
                }   
            }
        } else {
            if(this.kag.tmp.ready_audio == false){
                $(".tyrano_base").on("click.bgm", function() {
                        that.kag.readyAudio();
                        that.kag.tmp.ready_audio = true;
                        that.play(pm);
                        $(".tyrano_base").off("click.bgm");
                });
            }else{
                that.play(pm);
            }   
        }
    },
    play : function(pm) {
        var that = this;
        var target = "bgm";
        if (pm.target == "se") {
            target = "sound";
            this.kag.tmp.is_se_play = true;
            //指定されたbufがボイス用に予約されてるか否か
            if(this.kag.stat.map_vo["vobuf"][pm.buf]){
                this.kag.tmp.is_vo_play = true;
            }
            //ループ効果音の設定
            if(!this.kag.stat.current_se){
                this.kag.stat.current_se = {};    
            }
            if(pm.stop == "false") {
                if(pm.loop=="true"){
                    this.kag.stat.current_se[pm.buf] = pm;
                }else{
                    if(this.kag.stat.current_se[pm.buf]){
                        delete this.kag.stat.current_se[pm.buf];
                    }
                }
            }
        }else{
            //追加
            that.kag.tmp.map_is_audio_stopping[pm.target + pm.buf] = false
            this.kag.tmp.is_audio_stopping = false;
            this.kag.tmp.is_bgm_play = true;
        }
        var volume = 1;
        if (pm.volume !== "") {
            volume = parseFloat(parseInt(pm.volume) / 100);
        }
        var ratio = 1;
        //デフォルトで指定される値を設定
        if (target === "bgm") {
            if (typeof this.kag.config.defaultBgmVolume == "undefined") {
                ratio = 1;
            } else {
                ratio = parseFloat(parseInt(this.kag.config.defaultBgmVolume) / 100);
            }
            //bufが指定されていて、かつ、デフォルトボリュームが指定されている場合は
            if(typeof this.kag.stat.map_bgm_volume[pm.buf] !="undefined"){
                ratio = parseFloat(parseInt(this.kag.stat.map_bgm_volume[pm.buf])/100);
            }
        } else {
            if (typeof this.kag.config.defaultSeVolume == "undefined") {
                ratio = 1;
            } else {
                ratio = parseFloat(parseInt(this.kag.config.defaultSeVolume) / 100);
            }
            //bufが指定されていて、かつ、デフォルトボリュームが指定されている場合は
            if(typeof this.kag.stat.map_se_volume[pm.buf] != "undefined"){
                ratio = parseFloat(parseInt(this.kag.stat.map_se_volume[pm.buf])/100);
            }
        }
        volume *= ratio;
        var storage_url = "";
        var browser = $.getBrowser();
        var storage = pm.storage;

        //ogg m4a を推奨するための対応 ogg を m4a に切り替え
        //mp3 が有効になっている場合は無視する
        if (this.kag.config.mediaFormatDefault != "mp3") {
            if (browser == "msie" || browser == "safari" || browser=="edge") {
                storage = $.replaceAll(storage, ".ogg", ".m4a");
            }
        }
        if ($.isHTTP(pm.storage)) {
            storage_url = storage;
        } else {
            if(storage!=""){
                storage_url = "./data/" + target + "/" + storage;
            }else{
                storage_url ="";
            }
        }

        //音楽再生
        var audio_obj =null ;
        var howl_opt = {
            src: storage_url,
            volume:(volume),
            onend:(e)=>{
                //this.j_btnPreviewBgm.parent().removeClass("soundOn");   
            }
        };
        //スプライトが指定されている場合
        if(pm.sprite_time!=""){
            let array_sprite = pm.sprite_time.split("-");
            let sprite_from = parseInt($.trim(array_sprite[0]));
            let sprite_to = parseInt($.trim(array_sprite[1]));
            let duration = sprite_to - sprite_from;
            howl_opt["sprite"] = {"sprite_default":[sprite_from, duration, $.toBoolean(pm.loop) ]}
        }
        audio_obj = new Howl(howl_opt);
        if (pm.loop == "true") {
            audio_obj.loop(true);
        }else{
            audio_obj.loop(false);
        }
        if (target === "bgm") {
            if(this.kag.tmp.map_bgm[pm.buf]){
                this.kag.tmp.map_bgm[pm.buf].unload();
            }
            this.kag.tmp.map_bgm[pm.buf] = audio_obj;
            that.kag.stat.current_bgm = storage;
            that.kag.stat.current_bgm_vol = pm.volume;
        } else {
            //効果音の時はバッファ指定
            //すでにバッファが存在するなら、それを消す。
            if(this.kag.tmp.map_se[pm.buf]){
                this.kag.tmp.map_se[pm.buf].pause();
                this.kag.tmp.map_se[pm.buf].unload();
                delete this.kag.tmp.map_se[pm.buf] ;
            }
            this.kag.tmp.map_se[pm.buf] = audio_obj;
        }
        audio_obj.once("play",function(){
            that.kag.layer.showEventLayer();
			if (pm.stop == "false") {
				that.kag.ftag.nextOrder();
            }
        });
        var id = null
        if(pm.sprite_time!==""){
            id = audio_obj.play("sprite_default");
        }else{
            id = audio_obj.play();
        }
        //追加
        that.kag.tmp.map_audio_id[pm.target + pm.buf] = id
        if (pm.fadein == "true") {
            audio_obj.fade(0, volume, parseInt(pm.time));
        }
        //再生が完了した時
        audio_obj.on("end",function(e){
            if (pm.target == "se") {
                that.kag.tmp.is_se_play = false;
                that.kag.tmp.is_vo_play = false;
                if(that.kag.tmp.is_se_play_wait == true){
                    that.kag.tmp.is_se_play_wait = false;
                    that.kag.ftag.nextOrder();
                }else if(that.kag.tmp.is_vo_play_wait==true){
                    that.kag.tmp.is_vo_play_wait = false;
                    setTimeout(function(){
                        that.kag.ftag.nextOrder();
                    },500);
                }
            }else if(pm.target == "bgm") {
                that.kag.tmp.is_bgm_play = false;
                if(that.kag.tmp.is_bgm_play_wait == true){
                    that.kag.tmp.is_bgm_play_wait = false;
                    that.kag.ftag.nextOrder();
                }
            }
        });
    },
})
tyrano.plugin.kag.ftag.master_tag.playbgm = tyrano.plugin.kag.tag.playbgm
tyrano.plugin.kag.ftag.master_tag.playbgm.kag = _kag
*/

//[wait]タグをクリックでウェイトキャンセル有効に
var _wait = tyrano.plugin.kag.tag.wait
var _kag = tyrano.plugin.kag.ftag.master_tag.wait.kag
tyrano.plugin.kag.tag.wait = $.extend(true, {} , _wait, {
    pm: {
        click: "true"
    },
    start: function (pm) {
        var that = this;

        //スキップ中
        if(tyrano.plugin.kag.stat.is_skip == true){
            pm.time = 0
        }

        if(pm.click === "true"){
            $("#tyrano_base").on("click.waitcancel", function(){
                clearTimeout(that.kag.tmp.wait_id);
                that.kag.stat.is_strong_stop = false;
                that.kag.stat.is_wait = false;
                that.kag.layer.showEventLayer();
                that.kag.ftag.nextOrder()
                $("#tyrano_base").off('click.waitcancel');
                return false
            })
        }
        
        that.kag.stat.is_strong_stop = true;
        that.kag.stat.is_wait = true;
        that.kag.layer.hideEventLayer();
        that.kag.tmp.wait_id = setTimeout(function () {
            that.kag.stat.is_strong_stop = false;
            that.kag.stat.is_wait = false;
            that.kag.layer.showEventLayer();
            $("#tyrano_base").off('click.waitcancel');    //確実に消す
            that.kag.ftag.nextOrder()
        }, pm.time)
    }.bind(TYRANO)
})
tyrano.plugin.kag.ftag.master_tag.wait = tyrano.plugin.kag.tag.wait
tyrano.plugin.kag.ftag.master_tag.wait.kag = _kag
*/

//[wbgm]時にクリックで強制終了
var _wbgm = tyrano.plugin.kag.tag.wbgm 
var _kag = tyrano.plugin.kag.ftag.master_tag.wbgm.kag
tyrano.plugin.kag.tag.wbgm = $.extend(true, {}, _wbgm, {
    pm: {
        click: "false"
    },
    start: function (pm) {
        var that = this
        //スキップ中
        if(tyrano.plugin.kag.stat.is_skip === true){
            this.kag.ftag.startTag("stopbgm")
        }

        if(pm.click === "true"){
            $("#tyrano_base").on("click._wbgm", function(){
                var _pm = {
                    loop: "true",
                    storage: "",
                    fadeout: "false",
                    time: 0
                }
                that.kag.ftag.startTag("stopbgm", _pm)
                $("#tyrano_base").off("click._wgbm")
            })
        }

        if (this.kag.tmp.is_bgm_play == true){
            this.kag.tmp.is_bgm_play_wait = true;
        }else{
            $("#tyrano_base").off("click._wgbm")
            this.kag.ftag.nextOrder()
        } 

    }.bind(TYRANO)
})
tyrano.plugin.kag.ftag.master_tag.wbgm = tyrano.plugin.kag.tag.wbgm
tyrano.plugin.kag.ftag.master_tag.wbgm.kag = _kag
*/

//[wse]時にクリックで強制終了

var _wse = tyrano.plugin.kag.tag.wse 
var _kag = tyrano.plugin.kag.ftag.master_tag.wse.kag
tyrano.plugin.kag.tag.wse = $.extend(true, {}, _wse, {
    pm: {
        click: "false"
    },
    start: function (pm) {
        var that = this
        //スキップ中
        if(tyrano.plugin.kag.stat.is_skip === true){
            this.kag.ftag.startTag("stopbgm")
        }

        if(pm.click === "true"){
            $("#tyrano_base").on("click._wse", function(){
                var _pm = {
                    storage: "",
                    target: "se",
                    loop: "false",
                    buf: "0",
                    fadeout: "true",
                    time: 0
                }
                that.kag.ftag.startTag("stopbgm", _pm)
                $("#tyrano_base").off("click._wse")
            })
        }

        if (this.kag.tmp.is_se_play == true){
            this.kag.tmp.is_se_play_wait = true;
        }else{
            $("#tyrano_base").off("click._wse")
            this.kag.ftag.nextOrder()
        }   
    }.bind(TYRANO)
})
tyrano.plugin.kag.ftag.master_tag.wse = tyrano.plugin.kag.tag.wse
tyrano.plugin.kag.ftag.master_tag.wse.kag = _kag

//マスク中にクリックでトランジション終了
var _mask = tyrano.plugin.kag.tag.mask
var _kag = tyrano.plugin.kag.ftag.master_tag.mask.kag
tyrano.plugin.kag.tag.mask = $.extend(true, {}, _mask, {
    pm: {
        click: "true"
    },
    start: function (pm) {
        //スキップ中
        if(tyrano.plugin.kag.stat.is_skip === true){
            pm.time = 0
        }

        var that = this;
        that.kag.layer.hideEventLayer();
        var j_div = $("<div class='layer layer_mask' data-effect='" + pm.effect + "' style='z-index:100000000;position:absolute;'>");
        j_div.css("animation-duration", parseInt(pm.time) + "ms");
        var sc_width = parseInt(that.kag.config.scWidth);
        var sc_height = parseInt(that.kag.config.scHeight);
        j_div.css({
            width: sc_width,
            height: sc_height,
            opacity: 0,
        });
        if (pm.color == "none") {
            j_div.css("background-color", "");
        }else{
            j_div.css("background-color", $.convertColor(pm.color));
        }
        if (pm.graphic != "") {
            if (pm.folder != "") {
                folder = pm.folder;
            }else{ 
                folder = "image";
            }
            var storage_url = "";
            if (pm.graphic != "") {
                storage_url = "./data/" + folder + "/" + pm.graphic;
                j_div.css("background-image", "url(" + storage_url + ")")
            }
        }
        // ここから追加
        if(pm.click == "true"){
            j_div.on("click._mask",function(){
                j_div.css("animation-duration", "0ms");
                j_div.off("click._mask")
            })
        }
        // ここまで追加

        $(".tyrano_base").append(j_div);

        var animationEnd = "webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend";
        j_div.addClass("animated " + pm.effect).one(animationEnd, function () {
            $(".tyrano_base").off("click._mask")    //追加
                that.kag.ftag.nextOrder()
        })
    }.bind(TYRANO)
})
tyrano.plugin.kag.ftag.master_tag.mask = tyrano.plugin.kag.tag.mask
tyrano.plugin.kag.ftag.master_tag.mask.kag = _kag


//[mask_off]時クリックで終了
var _mask_off = tyrano.plugin.kag.tag.mask_off
var _kag = tyrano.plugin.kag.ftag.master_tag.mask_off.kag
tyrano.plugin.kag.tag.mask_off = $.extend(true, {}, _mask_off, {
    vital: [],
    pm: {
        click: "true"
    },
    start: function (pm) {
        var that = this;
        var j_div = $(".layer_mask");
        //スキップ中
        if(tyrano.plugin.kag.stat.is_skip === true){
            pm.time = 0
        }
    
        if (j_div.get(0)) {

            //ここから追加
            if(pm.click == "true"){
                $(".layer_mask").on("click._mask_off", function(){
                    j_div.css("animation-duration", "0ms");
                    $(".layer_mask").off("click._mask_off")
                })
            }
            //ここまで追加

            var _effect = j_div.attr("data-effect");
            j_div.removeClass("animated " + _effect);
            j_div.css("animation-duration", parseInt(pm.time) + "ms");
            var animationEnd = "webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend";
            j_div.addClass("animated " + pm.effect).one(animationEnd, function () {
                j_div.remove();
                that.kag.layer.showEventLayer();
                $(".layer_mask").off("click._mask_off")  //追加
                that.kag.ftag.nextOrder()
            })
        } else {
            that.kag.layer.showEventLayer();
                that.kag.ftag.nextOrder()
        }
    }.bind(TYRANO)
})
tyrano.plugin.kag.ftag.master_tag.mask_off = tyrano.plugin.kag.tag.mask_off
tyrano.plugin.kag.ftag.master_tag.mask_off.kag = _kag


//chara_ptext拡張
var _chara_ptext = tyrano.plugin.kag.tag.chara_ptext
var _kag = tyrano.plugin.kag.ftag.master_tag.chara_ptext.kag
tyrano.plugin.kag.tag.chara_ptext = $.extend(true, {}, _chara_ptext, {
    pm : {
        name : "",
        face : "",
        jname: "",
    },
    start : function(pm) {
        var that = this;
        this.kag.layer.hideEventLayer();
        if (pm.name == "") {
            $("." + this.kag.stat.chara_ptext).html("");
            //全員の明度を下げる。誰も話していないから
            //明度設定が有効な場合
            if (this.kag.stat.chara_talk_focus != "none") {
                $("#tyrano_base").find(".tyrano_chara").css({
                    "-webkit-filter" : this.kag.stat.apply_filter_str,
                    "-ms-filter" : this.kag.stat.apply_filter_str,
                    "-moz-filter" : this.kag.stat.apply_filter_str
                });
            }
        } else {
            //日本語から逆変換することも可能とする
            if(this.kag.stat.jcharas[pm.name]){
                pm.name = this.kag.stat.jcharas[pm.name];
            }
            var cpm = this.kag.stat.charas[pm.name];
            if (cpm) {

                //ここから追加
                //jnameが[chara_ptext]で指定されている場合、そちらを優先して表示
                if(pm.jname){
                    var jname = pm.jname
                }else{
                    var jname = cpm.jname
                }
                //キャラクター名出力
                $("." + this.kag.stat.chara_ptext).html(jname);
                //ここまで追加

                //色指定がある場合は、その色を指定する。
                if (cpm.color != "") {
                    $("." + this.kag.stat.chara_ptext).css("color", $.convertColor(cpm.color));
                }
                //明度設定が有効な場合
                if (this.kag.stat.chara_talk_focus != "none") {
                    $("#tyrano_base").find(".tyrano_chara").css({
                        "-webkit-filter" : this.kag.stat.apply_filter_str,
                        "-ms-filter" : this.kag.stat.apply_filter_str,
                        "-moz-filter" : this.kag.stat.apply_filter_str
                    });
                    $("#tyrano_base").find("." + pm.name + ".tyrano_chara").css({
                        "-webkit-filter" : "brightness(100%) blur(0px)",
                        "-ms-filter" : "brightness(100%) blur(0px)",
                        "-moz-filter" : "brightness(100%) blur(0px)"
                    });
                }
                
                //指定したキャラクターでアニメーション設定があった場合
                if(this.kag.stat.chara_talk_anim != "none"){                    
                    var chara_obj = $("#tyrano_base").find("." + pm.name + ".tyrano_chara");
                    if(chara_obj.get(0)){                        
                        this.animChara(chara_obj, this.kag.stat.chara_talk_anim, pm.name);                        
                        if (pm.face != "") {
                            //即表情変更、アニメーション中になるから        
                            this.kag.ftag.startTag("chara_mod", {name:pm.name,face:pm.face,time:"0"});
                        }
                    }
                }
            } else {
                //存在しない場合はそのまま表示できる
                $("." + this.kag.stat.chara_ptext).html(pm.name);
                //存在しない場合は全員の明度を下げる。
                if (this.kag.stat.chara_talk_focus != "none") {
                    $("#tyrano_base").find(".tyrano_chara").css({
                        "-webkit-filter" : this.kag.stat.apply_filter_str,
                        "-ms-filter" : this.kag.stat.apply_filter_str,
                        "-moz-filter" : this.kag.stat.apply_filter_str
                    });
                }       
            }
        }
        //ボイス設定が有効な場合
        if(this.kag.stat.vostart == true){
            //キャラクターのボイス設定がある場合            
            if(this.kag.stat.map_vo["vochara"][pm.name]){                
                var vochara = this.kag.stat.map_vo["vochara"][pm.name];      
                //ここから変更         
                var num =  ("000" + vochara.number).slice(-3)
                var playsefile = $.replaceAll(vochara.vostorage,"{number}",num);                
                //ここまで変更
                var se_pm = {
                    loop : "false",
                    storage : playsefile,
                    stop : "true",
                    buf:vochara.buf
                };
                //ここから変更         
                if(this.kag.stat.is_skip != true){
                    vo = this.kag.stat.map_vo["vochara"]
                    Object.keys(vo).forEach(k => {
                        this.kag.ftag.startTag("stopse", {
                            buf: vo[k].buf,
                            stop: "true",
                            time: 0,
                        });
                    })
                    //ここまで変更
                    this.kag.ftag.startTag("playse", se_pm);
                }
                this.kag.stat.map_vo["vochara"][pm.name]["number"] = parseInt(vochara.number)+1;                
            }            
        }
        this.kag.stat.f_chara_ptext="true";
        
        //表情の変更もあわせてできる
        if (pm.face != "") {
            if (!(this.kag.stat.charas[pm.name]["map_face"][pm.face])) {
                this.kag.error("指定されたキャラクター「" + pm.name + "」もしくはface:「" + pm.face + "」は定義されていません。もう一度確認をお願いします");
                return;
            }
            var storage_url = this.kag.stat.charas[pm.name]["map_face"][pm.face];
            //chara_mod タグで実装するように調整
            if(this.kag.stat.chara_talk_anim == "none"){
                var time = TYRANO.kag.stat.chara_time
                if(TYRANO.kag.stat.is_skip == true){
                    time = "0"
                }
                this.kag.ftag.startTag("chara_mod", {
                    name: pm.name,
                    face: pm.face,
                    cross: "false",
                    time: time,
                });
            }
        }else{
            this.kag.layer.showEventLayer();
            this.kag.ftag.nextOrder();        
        }
    }.bind(TYRANO)
})
tyrano.plugin.kag.ftag.master_tag.chara_ptext = tyrano.plugin.kag.tag.chara_ptext
tyrano.plugin.kag.ftag.master_tag.chara_ptext.kag = _kag


//オーディオ停止拡張
var _stopbgm = tyrano.plugin.kag.tag.stopbgm
var _kag = tyrano.plugin.kag.ftag.master_tag.stopbgm.kag
tyrano.plugin.kag.tag.stopbgm = $.extend(true, {}, _stopbgm, {
    pm : {
        fadeout : "false",
        time : 2000,
        target : "bgm",
        buf:"0",
        stop : "false" //trueの場合自動的に次の命令へ移動しない。ロード対策
    },
    start : function(pm) {
        var that = this;
        var target_map = null;
        if (pm.target == "bgm") {
            target_map = this.kag.tmp.map_bgm;
            that.kag.tmp.is_bgm_play = false;
            that.kag.tmp.is_bgm_play_wait = false;
        } else {
            target_map = this.kag.tmp.map_se;
            that.kag.tmp.is_vo_play = false;
            that.kag.tmp.is_se_play = false;
            that.kag.tmp.is_se_play_wait = false;
            if(!this.kag.stat.current_se){
                this.kag.stat.current_se = {};    
            }
            if(pm.stop == "false") {
                if(this.kag.stat.current_se[pm.buf]){
                    delete this.kag.stat.current_se[pm.buf];
                }
            }
        }
        var browser = $.getBrowser();
        var isFind = false
        for (key in target_map ) {
            if(key==pm.buf){
                (function() {
                    isFind = true
                    var _key = key;
                    var _audio_obj = null;
                    if (pm.target === "bgm") {
                        _audio_obj = target_map[_key];
                        //ロード画面の場合、再生中の音楽はそのまま、直後にロードするから
                        if (pm.stop == "false") {
                            that.kag.stat.current_bgm = "";
                            that.kag.stat.current_bgm_vol = "";
                        }
                    } else {
                        _audio_obj = target_map[_key];
                    }
                    _audio_obj.on("fade", function(){
                        that.kag.tmp.map_is_audio_stopping[pm.target + pm.buf] = false
                        if(pm.stop == "false" && that.kag.tmp.map_is_audio_wait[pm.target + pm.buf] == true){
                            that.kag.tmp.map_is_audio_wait[pm.target + pm.buf] = false
                            that.kag.ftag.nextOrder();    
                        }
                    })
                    //フェードアウトしながら再生停止
                    if (pm.fadeout == "true") {
                        _audio_obj.fade(_audio_obj.volume(), 0, parseInt(pm.time));
                        that.kag.tmp.map_is_audio_stopping[pm.target + pm.buf] = true
                    } else {
                        _audio_obj.stop();
                    }
                })();
            }
        }
        if (pm.stop == "false"){
//            if(isFind == false){
//                this.kag.ftag.nextOrder();
//            }else if(pm.fadeout == "false") {
//                this.kag.ftag.nextOrder();
//            }else if(pm.fadeout == "true") {
                this.kag.ftag.nextOrder();
//            }
        }
    }.bind(TYRANO)
});
tyrano.plugin.kag.ftag.master_tag.stopbgm = tyrano.plugin.kag.tag.stopbgm
tyrano.plugin.kag.ftag.master_tag.stopbgm.kag = _kag

//オーディオフェードアウト待ち
tyrano.plugin.kag.tag.wfade = {
    vital: [],
    pm: {
        target: "bgm",
        buf: 0
    },
    start: function (pm) {
        that = TYRANO
        if (that.kag.tmp.map_is_audio_stopping[pm.target + pm.buf] === undefined || that.kag.tmp.map_is_audio_stopping[pm.target + pm.buf] === false) {
            that.kag.ftag.nextOrder()
        }else{
            that.kag.tmp.map_is_audio_wait[pm.target + pm.buf] = true
        }
    }
};
tyrano.plugin.kag.ftag.master_tag.wfade = tyrano.plugin.kag.tag.wfade
tyrano.plugin.kag.ftag.master_tag.wfade.kag = tyrano.plugin.kag


//オートモード時に画像表示
/*
var _autostart = tyrano.plugin.kag.tag.autostart
var _kag = tyrano.plugin.kag.ftag.master_tag.autostart.kag
tyrano.plugin.kag.tag.autostart = $.extend(true, {}, _autostart, {
    start: function (pm) {
        _autostart.start.apply(TYRANO, arguments)
        TYRANO.kag.ftag.startTag("image", {
            storage: "is_auto.png",
            layer: "2",
            visible: "true",
            x: "1060",
            y: "20",
            name: "is_auto",
            zindex: 9999999999
        })
    }.bind(TYRANO)
})
tyrano.plugin.kag.ftag.master_tag.autostart = tyrano.plugin.kag.tag.autostart
tyrano.plugin.kag.ftag.master_tag.autostart.kag = _kag
*/

//オートモード終了時に画像消す
/*
var _autostop = tyrano.plugin.kag.tag.autostop
var _kag = tyrano.plugin.kag.ftag.master_tag.autostop.kag
tyrano.plugin.kag.tag.autostop = $.extend(true, {}, _autostop, {
    start: function (pm) {
        _autostop.start.apply(TYRANO, arguments)
        $(".is_auto").remove()
    }.bind(TYRANO)
})
tyrano.plugin.kag.ftag.master_tag.autostop = tyrano.plugin.kag.tag.autostop
tyrano.plugin.kag.ftag.master_tag.autostop.kag = _kag
*/

/*
//スキップスタート時に画像表示
var _skipstart = tyrano.plugin.kag.tag.skipstart
var _kag = tyrano.plugin.kag.ftag.master_tag.skipstart.kag
tyrano.plugin.kag.tag.skipstart = $.extend(true, {}, _skipstart, {
    start: function (pm) {
        _skipstart.start.apply(TYRANO, arguments)
        TYRANO.kag.ftag.startTag("image", {
            storage: "is_skip.png",
            layer: "2",
            visible: "true",
            x: "1060",
            y: "20",
            name: "is_skip",
            zindex: 9999999999
        })
    }.bind(TYRANO)
})
tyrano.plugin.kag.ftag.master_tag.skipstart = tyrano.plugin.kag.tag.skipstart
tyrano.plugin.kag.ftag.master_tag.skipstart.kag = _kag

//スキップ終了時に画像消す
var _skipstop = tyrano.plugin.kag.tag.skipstop
var _kag = tyrano.plugin.kag.ftag.master_tag.skipstop.kag
tyrano.plugin.kag.tag.skipstop = $.extend(true, {}, _skipstop, {
    start: function (pm) {
        _skipstop.start.apply(TYRANO, arguments)
        $(".is_skip").remove()
    }.bind(TYRANO)
})
tyrano.plugin.kag.ftag.master_tag.skipstop = tyrano.plugin.kag.tag.skipstop
tyrano.plugin.kag.ftag.master_tag.skipstop.kag = _kag

//スキップキャンセル　その場に留まる
var _cancelskip = tyrano.plugin.kag.tag.cancelskip
var _kag = tyrano.plugin.kag.ftag.master_tag.cancelskip.kag
tyrano.plugin.kag.tag.cancelskip = $.extend(true, {}, _cancelskip, {
    start: function (pm) {
        //this.kag.stat.is_skip = false;
        _cancelskip.start.apply(TYRANO, arguments)
        $(".is_skip").remove()
    }.bind(TYRANO)
})
tyrano.plugin.kag.ftag.master_tag.cancelskip = tyrano.plugin.kag.tag.cancelskip
tyrano.plugin.kag.ftag.master_tag.cancelskip.kag = _kag

//[l]　スキップ終了判定
var _l = tyrano.plugin.kag.tag.l
var _kag = tyrano.plugin.kag.ftag.master_tag.l.kag
tyrano.plugin.kag.tag.l = $.extend(true, {}, _l, {
    start: function (pm) {
        if (this.kag.stat.is_skip == false){
            $(".is_skip").remove()
        }
        _l.start.apply(TYRANO, arguments)
    }.bind(TYRANO)
})
tyrano.plugin.kag.ftag.master_tag.l = tyrano.plugin.kag.tag.l
tyrano.plugin.kag.ftag.master_tag.l.kag = _kag

//[p]　スキップ終了判定
var _p = tyrano.plugin.kag.tag.p
var _kag = tyrano.plugin.kag.ftag.master_tag.p.kag
tyrano.plugin.kag.tag.p = $.extend(true, {}, _p, {
    start: function (pm) {
        if (this.kag.stat.is_skip == false){
            $(".is_skip").remove()
        }
        _p.start.apply(TYRANO, arguments)
    }.bind(TYRANO)
})
tyrano.plugin.kag.ftag.master_tag.p = tyrano.plugin.kag.tag.p
tyrano.plugin.kag.ftag.master_tag.p.kag = _kag
*/

