;一番最初に呼び出されるファイル

[title name="乙女戦隊"]

[stop_keyconfig]


;ティラノスクリプトが標準で用意している便利なライブラリ群
;コンフィグ、CG、回想モードを使う場合は必須
@call storage="tyrano.ks"
[loadcss file="data/others/custum.css"]

;ゲームで必ず必要な初期化処理はこのファイルに記述するのがオススメ

;メッセージボックスは非表示
@layopt layer="message" visible=false

;最初は右下のメニューボタンを非表示にする
[hidemenubutton]

;テスト用
[plugin name=tsex]

;[showmenu]
;タイトル画面へ移動
@jump storage="scene1.ks"

[s]


